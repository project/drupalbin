DrupalBin Install Profile
=========================

DrupalBin is a tool to collaborate and debug code snippets.  Its goal is
to replicate what is accomplished by various pastebin-like websites.  More on
DrupalBin is available at http://drupalbin.com .


Branch
------

Please checkout the branch according to what version of Drupal you wish to run:

Drupal 5
  DRUPAL-5

Drupal 6
  DRUPAL-6--1
